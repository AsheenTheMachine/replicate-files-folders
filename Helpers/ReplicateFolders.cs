﻿using System;
using System.IO;
using System.Linq;

namespace Replicate_Folders_Files.Helpers
{
    public delegate void CopyingHandler(string message);
    public delegate void ProgressHandler(int progressValue);

    public class ReplicateFolders
    {
        public event CopyingHandler FileUpdate;
        public event ProgressHandler ProgressBarUpdate;

        int totalFileCount, totalFilesComplete;
        bool copyToTarget, totalFileContSet;

        FileInfo sourceFile, targetFile;
        FileInfo [] fileInfoArr;
        DirectoryInfo [] targetDirArr;
        DirectoryInfo [] sourceDirInfo;

        public string ProcessFolderReplication(DirectoryInfo source, DirectoryInfo target, bool includeSubFolders, bool doNotDelete)
        {
            try
            {
                if (source.FullName.ToLower() == target.FullName.ToLower())
                    return "Same directory";

                #region Total Number of Fiels and Log File Info

                //get total number of files
                if(!totalFileContSet)
                {
                    SetTotalFilesCount(source.FullName, includeSubFolders);

                    FileUpdate?.Invoke(string.Format(@"Directory Replication"));
                    FileUpdate?.Invoke(string.Format(@"Start Date Time: {0}", DateTime.Now.ToString()));
                    FileUpdate?.Invoke(string.Format(@"Source Directory: {0}", source.ToString()));
                    FileUpdate?.Invoke(string.Format(@"Target Directory: {0}", target.ToString()));
                    FileUpdate?.Invoke(string.Format(@"========================================================"));
                    FileUpdate?.Invoke(string.Format(@"========================================================"));

                    FileUpdate?.Invoke(string.Format(""));
                    FileUpdate?.Invoke(string.Format(@"Total Number of Files to Copy {0}:", totalFileCount));
                    FileUpdate?.Invoke(string.Format(""));
                }

                #endregion

                if (Directory.Exists(target.FullName) == false)
                    Directory.CreateDirectory(target.FullName);

                #region Delete Files

                if(!doNotDelete)
                {                
                    //delete file from target if it does not exist in source
                    DirectoryInfo dir = new DirectoryInfo(target.ToString());
                    fileInfoArr = dir.GetFiles();

                    //loop through target files
                    for(int i = 0; i < fileInfoArr.Length; i++)
                    {
                        if(!File.Exists(Path.Combine(source.ToString(), fileInfoArr[i].Name)))
                        {
                            File.Delete(fileInfoArr[i].FullName);
                            FileUpdate?.Invoke(string.Format(@"Deleted file {0}\{1}.", target.FullName, fileInfoArr[i].Name));
                        }
                    }
                }

                #endregion

                #region Copy Files

                //copy files if they don't exist in the target folder
                fileInfoArr = source.GetFiles();

                for(int i = 0; i < fileInfoArr.Length; i++)
                {
                    //check if target file exists
                    if(File.Exists(Path.Combine(target.ToString(), fileInfoArr[i].Name)))
                    {
                        //check the size and date diff
                        sourceFile = new FileInfo(fileInfoArr[i].FullName);
                        targetFile = new FileInfo(Path.Combine(target.ToString(), fileInfoArr[i].Name));

                        if((sourceFile.Length != targetFile.Length) || (sourceFile.CreationTime != targetFile.CreationTime))
                            copyToTarget = true;
                    }
                    else
                        copyToTarget = true;

                    if(copyToTarget)
                    {
                        //copy file to target location and add log info 
                        FileUpdate?.Invoke(string.Format(@"Copying {0}\{1}.", source.FullName, fileInfoArr[i].Name));
                        fileInfoArr[i].CopyTo(Path.Combine(target.ToString(), fileInfoArr[i].Name), true);
                        FileUpdate?.Invoke(string.Format(@"Copied {0}\{1} successfully.", target.FullName, fileInfoArr[i].Name));
                    }

                    totalFilesComplete++;
                    ProgressBarUpdate?.Invoke((totalFilesComplete * 100) / totalFileCount);
                    copyToTarget = false;
                }

                #endregion

                //Directories
                
                sourceDirInfo = source.GetDirectories();

                #region Delete Directories

                if(!doNotDelete)
                {
                    //delete target dir if does not exist in source
                    targetDirArr = target.GetDirectories();
                            
                    //loop through target folders
                    for(int i = 0; i < targetDirArr.Length; i++)
                    {
                        //if directory does not exist in source, delete it from target
                        if(!Directory.Exists(Path.Combine(source.FullName, targetDirArr[i].Name)))
                        {
                            FileUpdate?.Invoke(string.Format(@"Deleted directory {0}", targetDirArr[i].FullName));
                            Directory.Delete(targetDirArr[i].FullName, true);
                        }
                    }
                }
                
                #endregion

                #region Sub Folders

                if(includeSubFolders)
                {
                    //loop thrpigh each sub directory in source folder
                    foreach (DirectoryInfo sourceSubDir in sourceDirInfo)
                    {
                        //create the sub folder and add log info
                        FileUpdate?.Invoke(string.Format(@"Creating Directory {0} to {1}", sourceSubDir.FullName, target.ToString()));
                        DirectoryInfo nextTargetSubDir = target.CreateSubdirectory(sourceSubDir.Name);
                        FileUpdate?.Invoke(string.Format(@"Created Directory {0} successfully", nextTargetSubDir.FullName));

                        //recursive call to process sub folder
                        ProcessFolderReplication(sourceSubDir, nextTargetSubDir, includeSubFolders, doNotDelete);
                    }
                }

                #endregion

                FileUpdate?.Invoke("");
                return "Success";
            }
            catch(Exception ex)
            {
                FileUpdate?.Invoke(ex.Message);
                return "Error";
            }
        }

        private void SetTotalFilesCount(string path, bool includeSubFolders)
        {
            SearchOption searchOption = includeSubFolders == true ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;
            totalFileCount = Directory.EnumerateFiles(path, "*", searchOption).Count();

            totalFileContSet = true;
        }
    }
}
