﻿using System;
using System.Windows.Forms;

namespace Replicate_Folders_Files.Helpers
{
    public static class ThreadHelper
    {
        public static void AddLogItem(ListView ctrl, string text)
        {
            ListViewItem lvi = new ListViewItem();
            lvi.Text = text;
            ctrl.Items.Add(lvi);
        }

        public static void UpdateProgressBar(ProgressBar progressBar, Label label, int progressValue)
        {
            progressBar.Value = progressValue;
            label.Text = String.Format("{0}% Complete", progressValue);
        }
    }
}
