﻿namespace Replicate_Folders_Files
{
    partial class Replicate
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();

            this.Source = new System.Windows.Forms.TextBox();
            this.Target = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.IncludeSubFolders = new System.Windows.Forms.CheckBox();
            this.DoNotDelete = new System.Windows.Forms.CheckBox();
            this.ReplicateButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.LogsList = new System.Windows.Forms.ListView();
            this.LogFilePath = new System.Windows.Forms.LinkLabel();
            this.LogFilePathLabel = new System.Windows.Forms.Label();
            this.ProgressBar = new System.Windows.Forms.ProgressBar();
            this.ProgressBarLabel = new System.Windows.Forms.Label();
            this.ExitButton = new System.Windows.Forms.Button();
            this.ColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Source
            // 
            this.Source.Location = new System.Drawing.Point(59, 9);
            this.Source.Name = "Source";
            this.Source.Text = "Click to select folder..";
            this.Source.ReadOnly = true;
            this.Source.Size = new System.Drawing.Size(351, 20);
            this.Source.TabIndex = 0;
            this.Source.Click += new System.EventHandler(this.Source_Click);
            // 
            // Target
            // 
            this.Target.Location = new System.Drawing.Point(59, 35);
            this.Target.Name = "Target";
            this.Target.Text = "Click to select folder..";
            this.Target.ReadOnly = true;
            this.Target.Size = new System.Drawing.Size(351, 20);
            this.Target.TabIndex = 1;
            this.Target.Click += new System.EventHandler(this.Target_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Source";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Target";
            // 
            // IncludeSubFolders
            // 
            this.IncludeSubFolders.Location = new System.Drawing.Point(59, 70);
            this.IncludeSubFolders.Name = "IncludeSubFolders";
            this.IncludeSubFolders.Size = new System.Drawing.Size(200, 15);
            this.IncludeSubFolders.TabIndex = 4;
            this.IncludeSubFolders.Text = "Include Sub Directories";
            // 
            // DoNotDelete
            // 
            this.DoNotDelete.Location = new System.Drawing.Point(59, 90);
            this.DoNotDelete.Name = "DoNotDelete";
            this.DoNotDelete.Size = new System.Drawing.Size(200, 15);
            this.DoNotDelete.TabIndex = 5;
            this.DoNotDelete.Text = "Do Not Delete";
            // 
            // ReplicateButton
            // 
            this.ReplicateButton.Location = new System.Drawing.Point(59, 120);
            this.ReplicateButton.Name = "ReplicateButton";
            this.ReplicateButton.Size = new System.Drawing.Size(75, 23);
            this.ReplicateButton.TabIndex = 6;
            this.ReplicateButton.Text = "&Replicate";
            this.ReplicateButton.UseVisualStyleBackColor = true;
            this.ReplicateButton.Click += new System.EventHandler(this.ReplicateButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.LogsList);
            this.groupBox1.Location = new System.Drawing.Point(18, 200);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(392, 212);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Logs";
            // 
            // LogsList
            // 
            this.LogsList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ColumnHeader});
            this.LogsList.HideSelection = false;
            this.LogsList.Location = new System.Drawing.Point(6, 19);
            this.LogsList.Name = "LogsList";
            this.LogsList.Size = new System.Drawing.Size(380, 187);
            this.LogsList.TabIndex = 8;
            this.LogsList.UseCompatibleStateImageBehavior = false;
            this.LogsList.View = System.Windows.Forms.View.Details;
            // 
            // LogFilePathLabel
            // 
            this.LogFilePathLabel.Location = new System.Drawing.Point(18, 430);
            this.LogFilePathLabel.Name = "LogFilePathLabel";
            this.LogFilePathLabel.Size = new System.Drawing.Size(392, 20);
            this.LogFilePathLabel.TabIndex = 9;
            this.LogFilePathLabel.Text = "Log File: ";
            this.LogFilePathLabel.Click += new System.EventHandler(this.LogFilePath_Click);
            // 
            // LogFilePath
            // 
            this.LogFilePath.Location = new System.Drawing.Point(65, 430);
            this.LogFilePath.Name = "LogFilePath";
            this.LogFilePath.Size = new System.Drawing.Size(300, 20);
            this.LogFilePath.TabIndex = 10;
            this.LogFilePath.Click += new System.EventHandler(this.LogFilePath_Click);
            // 
            // ProgressBar
            // 
            this.ProgressBar.Location = new System.Drawing.Point(18, 170);
            this.ProgressBar.Name = "ProgressBar";
            this.ProgressBar.Size = new System.Drawing.Size(392, 20);
            this.ProgressBar.TabIndex = 11;
            // 
            // ProgressBarLabel
            // 
            this.ProgressBarLabel.AutoSize = true;
            this.ProgressBarLabel.Location = new System.Drawing.Point(18, 155);
            this.ProgressBarLabel.Name = "ProgressBarLabel";
            this.ProgressBarLabel.Size = new System.Drawing.Size(100, 13);
            this.ProgressBarLabel.TabIndex = 12;
            this.ProgressBarLabel.Text = "";
            // 
            // ExitButton
            // 
            this.ExitButton.Location = new System.Drawing.Point(355, 450);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(55, 23);
            this.ExitButton.TabIndex = 13;
            this.ExitButton.Text = "E&xit";
            this.ExitButton.UseVisualStyleBackColor = true;
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // columnHeader
            // 
            this.ColumnHeader.Text = "Output";
            this.ColumnHeader.Width = 360;
            // 
            // Window
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(428, 480);
            this.MaximizeBox = false;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ReplicateButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.IncludeSubFolders);
            this.Controls.Add(this.DoNotDelete);
            this.Controls.Add(this.Target);
            this.Controls.Add(this.Source);
            this.Controls.Add(this.ProgressBar);
            this.Controls.Add(this.ProgressBarLabel);
            this.Controls.Add(this.LogFilePath);
            this.Controls.Add(this.LogFilePathLabel);
            this.Controls.Add(this.ExitButton);
            this.Name = "Replicate Window";
            this.Text = "Replicate Folders & Files";
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private System.Windows.Forms.TextBox Source;
        private System.Windows.Forms.TextBox Target;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox IncludeSubFolders;
        private System.Windows.Forms.CheckBox DoNotDelete;
        private System.Windows.Forms.Button ReplicateButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListView LogsList;
        private System.Windows.Forms.ProgressBar ProgressBar;
        private System.Windows.Forms.Label ProgressBarLabel;
        private System.Windows.Forms.ColumnHeader ColumnHeader;
        private System.Windows.Forms.LinkLabel LogFilePath;
        private System.Windows.Forms.Label LogFilePathLabel;
        private System.Windows.Forms.Button ExitButton;
    }
}

