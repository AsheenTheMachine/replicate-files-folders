# README #

C# WinForms app to replicate files and folders from source path to target path.

### Technology Stack & Features ###

1.	C#
2.	WinForms
3.	.Net 5.0
4.	Visual Studio Code
5.	Recursion
6.	Progress Bas
7.	Threading (delegates)


### How do I get set up? ###

* Open the project in Visual Studio Code
* Open a command pallete and navigate to your project folder
* Type `dotnet run` hit enter

### Who do I talk to? ###

* Repo owner asheenk@gmail.com
