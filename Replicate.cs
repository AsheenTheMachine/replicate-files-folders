﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;
using Replicate_Folders_Files.Helpers;

namespace Replicate_Folders_Files
{
    public partial class Replicate : Form
    {
        bool userSettingChange;
        public Replicate()
        {
            InitializeComponent();
            ColumnHeader.Width = -2;
            SetUserSetting();
        }

        public void FolderBrowserDialogUpdateTextBox(TextBox textBox)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    textBox.Text = fbd.SelectedPath;
                    userSettingChange = true;
                }
            }
        }
        private void Source_Click(object sender, EventArgs e)
        {
            FolderBrowserDialogUpdateTextBox(Source);
        }

        private void Target_Click(object sender, EventArgs e)
        {
            FolderBrowserDialogUpdateTextBox(Target);
        }

        private async void ReplicateButton_Click(object sender, EventArgs e)
        {
            ProgressBar.Value = 0;
            ProgressBarLabel.Text = "";
            LogsList.Items.Clear();            

            if ((Source.Text != "Click to select folder..") && 
                (Target.Text != "Click to select folder.."))
            {
                DirectoryInfo source = new DirectoryInfo(Source.Text);
                DirectoryInfo target = new DirectoryInfo(Target.Text);

                string result = "";
                ReplicateButton.Enabled = false;

                await Task.Run(() => CopyTask());
                void CopyTask()
                {
                    ReplicateFolders repFunc = new ReplicateFolders();
                    repFunc.FileUpdate += AddLogToListView;
                    repFunc.ProgressBarUpdate += UpdateProgressBar;
                    result = repFunc.ProcessFolderReplication(source, target, IncludeSubFolders.Checked, DoNotDelete.Checked);
                }
                
                ListViewItem lvi = new ListViewItem();

                if (result == "Success")
                {
                    lvi.Text = String.Format("Replication Completed. End Time: {0}", DateTime.Now.ToString());
                    LogsList.Items.Add(lvi);
                }
                else if (result == "Same directory")
                {
                    lvi.Text = "The directories chosen are the same.";
                    LogsList.Items.Add(lvi);
                }
                else if (result == "Error")
                {
                    lvi.Text = "There was an error.";
                    LogsList.Items.Add(lvi);
                }

                //Save logs to log file
                string logFilePath = String.Format(@"Logs\{0}_{1}.txt", source.Name, Guid.NewGuid().ToString()) ;
                using (StreamWriter writer = new StreamWriter(logFilePath))  
                {  
                    foreach(ListViewItem item in LogsList.Items)
                    {
                        writer.WriteLine(item.Text);                        
                    }
                }

                //display link to log file
                string absoluteFilePath = Path.Combine(Environment.CurrentDirectory, logFilePath);
                LogFilePath.Tag = absoluteFilePath;
                LogFilePath.Text = Path.GetFileName(absoluteFilePath);
            }
            else
            {
                ListViewItem lvi = new ListViewItem();
                lvi.Text = "Folder not chosen.";
                LogsList.Items.Add(lvi);
            }

            //save user selection
            if(userSettingChange)
                SaveUserSetting();

            ReplicateButton.Enabled = true;
        }

        void LogFilePath_Click(object sender, EventArgs e)
        {
            //open log text file
            Process.Start("notepad.exe", (string)LogFilePath.Tag);
        }

        //save user settings to registry under current user
        private void SaveUserSetting()
        {
            RegistryKey rkSource = Registry.CurrentUser.CreateSubKey("Replicate_SourcePath");
            rkSource.SetValue("path", this.Source.Text);
            rkSource.Close();

            RegistryKey rkTarget = Registry.CurrentUser.CreateSubKey("Replicate_TargetPath");
            rkTarget.SetValue("path", this.Target.Text);
            rkTarget.Close();
        }

        //set screen as per users last input
        private void SetUserSetting()
        {
            try
            {
                RegistryKey rkSource = Registry.CurrentUser.OpenSubKey("Replicate_SourcePath", true);
                Source.Text = rkSource.GetValue("path").ToString();
                rkSource.Close();

                RegistryKey rkTarget = Registry.CurrentUser.OpenSubKey("Replicate_TargetPath", true);
                Target.Text = rkTarget.GetValue("path").ToString();
                rkTarget.Close();
            }
            catch{}
        }

        public void AddLogToListView(string message)
        {
            ThreadHelper.AddLogItem(LogsList, message);
        }

        public void UpdateProgressBar(int progressValue)
        {
            ThreadHelper.UpdateProgressBar(ProgressBar, ProgressBarLabel, progressValue);
        }
        
        private void ExitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
